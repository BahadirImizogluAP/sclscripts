USE tussentijdseevaluatie;
SET SQL_SAFE_UPDATES = 0;
LOCK TABLES vogels WRITE;
ALTER TABLE vogels;
UPDATE vogels SET klank='krr' WHERE soort='arend';
UPDATE vogels SET klank='roe' WHERE soort='duif';
set SQL_SAFE_UPDATES = 1;
UNLOCK TABLES;
