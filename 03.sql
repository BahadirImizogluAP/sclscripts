USE tussentijdseevaluatie;
CREATE TABLE IF NOT EXISTS Personen(
Voornaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Familienaam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Geboortedatum DATE,
GSMnummer INT UNSIGNED NOT NULL
);