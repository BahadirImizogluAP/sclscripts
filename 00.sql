DROP DATABASE IF EXISTS TussentijdseEvaluatie;
CREATE DATABASE TussentijdseEvaluatie;
USE TussentijdseEvaluatie;

CREATE TABLE Schilderijen (
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Artiest VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

CREATE TABLE Recepten (
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL
);

CREATE TABLE Boeken (
Titel VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Auteur VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
JaarVanUitgave YEAR,
Commentaar VARCHAR(100) 
);
INSERT INTO Boeken
VALUES
('De jaren van onschuld','Edith Wharton',1920,NULL),
('The Story of Doctor Doolittle','Hugh Lofting',1920,NULL),
('De woestijn van de Tartaren','Dino Buzzati',1940,NULL),
('To the Finland Station','Edmund Wilson',1940,NULL),
('Summerhill','A.S. Neill',1960,NULL);

CREATE TABLE Vogels (
Soort VARCHAR(100) CHAR SET utf8mb4 NOT NULL COLLATE utf8mb4_0900_as_cs,
Klank VARCHAR(50) CHAR SET utf8mb4 NOT NULL,
GewichtInGram MEDIUMINT,
Zwemt BOOLEAN,
Vliegt BOOLEAN
);
INSERT INTO Vogels
VALUES
('Aethopyga primigenia','ae',100,False,True),
('ara','pollywantacracker',70,False,True),
('duif','roekoe',30,False,True),
('kraai','caw',30,False,True),
('eend','kwaak',100,True,True),
('oehôe','oehoe',1000,False,True),
('arend','krrriii',300,False,True),
('pinguïn','kweh',100,True,False),
('zwaluw','swoosh',20,False,True),
('struisvogel','kweh',50000,False,False),
('emoe','kweh',30000,False,False);

CREATE TABLE Planten (
Soort VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
Categorie VARCHAR(20) NOT NULL,
GrondType VARCHAR(20) NOT NULL,
AangeradenGiettijd VARCHAR(50)
);
INSERT INTO Planten
VALUES
('den','naaldboom','zure grond',NULL),
('spar','naaldboom','zure grond','1 maand'),
('cactus','cactus','droge grond','2 maanden'),
('aloë vera','vetplant','droge grond',NULL),
('spekboom','vetplant','mineraalrijke grond','1 maand'),
('vrouwentong','vetplant','potgrond','1 week'),
('orchidee','sierplant','mineraalrijke grond','dagelijks');