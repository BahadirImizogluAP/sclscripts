USE ModernWays;
SELECT
avg(leeftijd) AS "Gemiddelde Leeftijd",
max(leeftijd) AS "Oudste dier",
count(*) AS "Hoeveel dieren"
FROM huisdieren;